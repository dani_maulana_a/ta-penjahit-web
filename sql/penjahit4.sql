-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2023 at 06:24 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `penjahit4`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahan_baku`
--

CREATE TABLE `bahan_baku` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_bahan_baku` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_bahanbaku` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga_beli` double DEFAULT NULL,
  `harga_jual` double DEFAULT NULL,
  `stok` double DEFAULT NULL,
  `satuan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_bahanbaku` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lebar` float NOT NULL,
  `warna_kain` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_id` bigint(20) DEFAULT NULL,
  `kolom_rak_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bahan_baku`
--

INSERT INTO `bahan_baku` (`id`, `kode_bahan_baku`, `nama_bahanbaku`, `harga_beli`, `harga_jual`, `stok`, `satuan`, `foto_bahanbaku`, `lebar`, `warna_kain`, `supplier_id`, `kolom_rak_id`, `created_at`, `updated_at`) VALUES
(18, 'B', 'kain balenciaga', 95000, NULL, 87, 'meter', '20230504222648.png', 1.5, 'Merah', 6, 12, '2023-03-21 06:20:49', '2023-05-05 13:18:46'),
(19, 'D', 'kain dior', 97000, NULL, 50, 'meter', '20230505200208.png', 1.5, 'Biru', 6, 13, '2023-03-21 06:22:37', '2023-05-05 13:19:17'),
(20, 'R', 'kain richmond', 120000, NULL, 70, 'meter', '20230505201537.png', 1.5, 'Hijau', 9, 15, '2023-03-21 06:22:53', '2023-05-05 13:15:38'),
(22, 'AG', 'kain alberto giovinco', 100000, NULL, 75.5, 'meter', '20230504222249.png', 1.5, 'Kuning', 11, 16, '2023-03-30 16:08:34', '2023-05-05 13:18:33'),
(23, 'BV', 'kain bulgari', 85000, NULL, 67, 'meter', '20230505195858.png', 1.5, 'Hijau', 13, 17, '2023-03-30 16:10:28', '2023-05-05 13:19:05'),
(24, 'C', 'kain chanel', 90000, NULL, 63, 'meter', '20230505200146.png', 1.5, 'Hitam', 14, 20, '2023-03-31 00:26:57', '2023-05-05 13:19:29'),
(27, 'H', 'Hermes', 100000, NULL, 58, 'meter', '20230504222224.png', 1.5, 'Hitam', 6, 20, '2023-04-18 09:19:43', '2023-05-05 13:18:09');

-- --------------------------------------------------------

--
-- Table structure for table `bom_model`
--

CREATE TABLE `bom_model` (
  `id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `bom_standart_ukuran_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bom_model`
--

INSERT INTO `bom_model` (`id`, `model_id`, `bom_standart_ukuran_id`, `created_at`, `updated_at`) VALUES
(1, 18, 1, '2023-04-21 07:42:50', '2023-04-21 07:42:50'),
(2, 18, 2, '2023-05-04 00:45:54', '2023-04-21 13:36:59'),
(3, 19, 2, '2023-04-29 06:39:43', '2023-04-29 06:39:43'),
(4, 19, 1, '2023-04-29 06:53:00', '2023-04-29 06:53:00'),
(7, 19, 3, '2023-05-04 03:27:41', '2023-05-04 03:27:41'),
(9, 18, 4, '2023-05-04 06:45:44', '2023-05-04 06:45:44'),
(10, 19, 4, '2023-05-04 07:25:50', '2023-05-04 07:25:50'),
(11, 20, 1, '2023-05-04 08:28:33', '2023-05-04 08:28:33'),
(12, 20, 4, '2023-05-04 08:54:05', '2023-05-04 08:54:05'),
(13, 18, 3, '2023-05-04 09:07:18', '2023-05-04 09:07:18'),
(14, 20, 2, '2023-05-17 04:49:42', '2023-05-17 04:49:42');

-- --------------------------------------------------------

--
-- Table structure for table `bom_model_detail`
--

CREATE TABLE `bom_model_detail` (
  `id` int(11) NOT NULL,
  `bom_id` int(11) NOT NULL,
  `bahanbaku_id` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bom_model_detail`
--

INSERT INTO `bom_model_detail` (`id`, `bom_id`, `bahanbaku_id`, `jumlah`, `created_at`, `updated_at`) VALUES
(1, 1, 25, 5, '2023-04-22 05:14:04', '2023-04-23 01:29:31'),
(2, 1, 19, 3, '2023-04-22 07:30:24', '2023-04-23 01:29:23'),
(3, 1, 18, 3, '2023-04-23 01:29:54', '2023-04-23 01:29:54'),
(4, 3, 23, 3, '2023-04-29 06:46:09', '2023-04-29 06:46:09'),
(5, 3, 25, 5, '2023-04-29 06:46:19', '2023-04-29 06:46:19'),
(6, 3, 26, 2, '2023-04-29 06:46:39', '2023-04-29 06:46:39'),
(7, 4, 23, 2.5, '2023-04-29 06:53:57', '2023-04-29 06:53:57'),
(8, 4, 25, 5, '2023-04-29 06:54:05', '2023-04-29 06:54:05'),
(9, 4, 26, 2, '2023-04-29 06:54:13', '2023-04-29 06:54:13'),
(10, 5, 18, 0, '2023-04-29 07:02:30', '2023-04-29 07:02:30'),
(11, 6, 29, 5, '2023-04-29 07:11:22', '2023-04-29 07:11:22'),
(12, 6, 30, 5, '2023-04-29 07:11:31', '2023-04-29 07:11:31'),
(13, 6, 25, 7, '2023-04-29 07:11:41', '2023-04-29 07:11:41'),
(14, 7, 23, 3, '2023-05-04 03:29:11', '2023-05-04 03:29:11'),
(16, 9, 20, 4, '2023-05-04 05:50:11', '2023-05-04 06:50:11'),
(17, 10, 22, 4, '2023-05-04 07:26:25', '2023-05-04 07:26:25'),
(18, 11, 18, 2, '2023-05-04 08:28:41', '2023-05-04 08:28:41'),
(19, 11, 26, 1, '2023-05-04 08:28:54', '2023-05-04 08:28:54'),
(20, 11, 25, 5, '2023-05-04 08:29:07', '2023-05-04 08:29:07'),
(21, 12, 18, 4, '2023-05-04 08:54:23', '2023-05-04 08:54:23'),
(22, 12, 26, 1, '2023-05-04 08:54:38', '2023-05-04 08:54:38'),
(23, 12, 25, 10, '2023-05-04 08:54:50', '2023-05-04 08:54:50'),
(24, 13, 18, 3.5, '2023-05-04 09:08:08', '2023-05-04 09:08:08');

-- --------------------------------------------------------

--
-- Table structure for table `bom_standart_ukuran`
--

CREATE TABLE `bom_standart_ukuran` (
  `id` int(11) NOT NULL,
  `ukuran` varchar(5) NOT NULL,
  `lebar_kain` double NOT NULL COMMENT 'meter',
  `panjang_kain` double NOT NULL COMMENT 'meter'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bom_standart_ukuran`
--

INSERT INTO `bom_standart_ukuran` (`id`, `ukuran`, `lebar_kain`, `panjang_kain`) VALUES
(1, 'S', 1.5, 3),
(2, 'M', 1.5, 3),
(3, 'L', 1.5, 3.5),
(4, 'XL', 1.5, 4),
(5, 'XXL', 1.5, 4.5);

-- --------------------------------------------------------

--
-- Table structure for table `detail_pembelian_bahanbaku`
--

CREATE TABLE `detail_pembelian_bahanbaku` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bahan_baku_id` bigint(20) DEFAULT NULL,
  `pembelian_bahanbaku_id` bigint(20) DEFAULT NULL,
  `jumlah` double DEFAULT NULL,
  `harga_beli` double DEFAULT NULL,
  `subtotal` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_pembelian_bahanbaku`
--

INSERT INTO `detail_pembelian_bahanbaku` (`id`, `bahan_baku_id`, `pembelian_bahanbaku_id`, `jumlah`, `harga_beli`, `subtotal`, `created_at`, `updated_at`) VALUES
(23, 18, 21, 45, 90000, 4050000, '2023-03-21 06:53:33', '2023-03-21 06:54:00'),
(24, 19, 21, 45, 97000, 4365000, '2023-03-21 07:01:03', '2023-03-21 07:02:18'),
(25, 20, 22, 45, 110000, 4950000, '2023-03-28 02:56:27', '2023-03-28 05:56:33'),
(26, 20, 22, 7, 100000, 700000, '2023-03-28 21:04:24', '2023-03-28 21:04:24'),
(33, 22, 29, 25, 100000, 2500000, '2023-03-30 16:15:28', '2023-03-30 16:15:28'),
(34, 23, 30, 45, 85000, 3825000, '2023-03-30 16:22:26', '2023-03-30 16:22:26'),
(35, 18, 31, 10, 90000, 900000, '2023-03-30 16:35:30', '2023-03-30 16:35:30'),
(37, 24, 33, 20, 90000, 1800000, '2023-03-31 00:35:22', '2023-03-31 00:35:22'),
(38, 24, 34, 10, 90000, 900000, '2023-03-31 00:36:15', '2023-03-31 00:36:15');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pemesanan_bahanbaku`
--

CREATE TABLE `detail_pemesanan_bahanbaku` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pemesanan_id` int(11) DEFAULT NULL,
  `bahan_baku_id` bigint(20) DEFAULT NULL,
  `detail_pemesanan_model_id` bigint(20) DEFAULT NULL,
  `ongkos_jahit` double DEFAULT NULL,
  `jumlah_terpakai` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_pemesanan_bahanbaku`
--

INSERT INTO `detail_pemesanan_bahanbaku` (`id`, `pemesanan_id`, `bahan_baku_id`, `detail_pemesanan_model_id`, `ongkos_jahit`, `jumlah_terpakai`, `created_at`, `updated_at`) VALUES
(30, 33, 19, 31, NULL, 20, '2023-03-22 19:35:02', '2023-03-28 23:22:08'),
(31, 33, 0, 0, NULL, 1, '2023-03-22 19:35:39', '2023-03-22 19:35:39'),
(32, 34, 19, 32, NULL, NULL, '2023-03-22 20:56:20', '2023-03-22 20:56:20'),
(38, 45, 18, 37, NULL, 6, '2023-03-29 22:19:44', '2023-03-30 21:18:45'),
(39, 46, 18, 38, NULL, 5.5, '2023-03-30 10:34:07', '2023-03-30 03:37:40'),
(40, 47, 20, 39, NULL, 5, '2023-03-31 00:21:23', '2023-03-30 17:29:50'),
(41, 48, 22, 40, NULL, 4.5, '2023-03-31 03:57:10', '2023-03-30 21:18:16'),
(42, 49, 20, 41, NULL, NULL, '2023-03-31 04:15:20', '2023-03-31 04:15:20'),
(43, 50, 18, 42, NULL, NULL, '2023-03-31 05:40:22', '2023-03-31 05:40:22'),
(44, 52, 18, 46, NULL, 4, '2023-04-29 06:04:07', '2023-04-29 07:04:25'),
(45, 55, 18, 48, NULL, NULL, '2023-05-02 06:32:23', '2023-05-02 06:32:23'),
(46, 56, 22, 49, NULL, NULL, '2023-05-02 06:42:16', '2023-05-02 06:42:16'),
(47, 58, 23, 51, NULL, NULL, '2023-05-04 02:31:41', '2023-05-04 02:31:41');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pemesanan_model`
--

CREATE TABLE `detail_pemesanan_model` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_id` bigint(20) DEFAULT NULL,
  `pemesanan_id` bigint(20) DEFAULT NULL,
  `jenis_model_id` bigint(20) DEFAULT 1,
  `banyaknya` double DEFAULT NULL,
  `ongkos_jahit` double DEFAULT NULL,
  `nama_model_detail` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_gambar` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi_pemesanan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_pemesanan_model`
--

INSERT INTO `detail_pemesanan_model` (`id`, `model_id`, `pemesanan_id`, `jenis_model_id`, `banyaknya`, `ongkos_jahit`, `nama_model_detail`, `file_gambar`, `deskripsi_pemesanan`, `created_at`, `updated_at`) VALUES
(31, 17, 33, 1, 10, 1000000, 'pegawai bca darmo', '20230322051607.png', NULL, '2023-03-21 22:16:07', '2023-03-21 22:16:07'),
(37, 18, 45, 1, 5, 1000000, '-', NULL, NULL, '2023-03-29 22:19:34', '2023-03-29 22:19:34'),
(38, 18, 46, 1, 5, 1000000, '-', NULL, NULL, '2023-03-30 10:33:30', '2023-03-30 10:33:30'),
(39, 18, 47, 1, 5, 1000000, '-', NULL, NULL, '2023-03-31 00:20:29', '2023-03-31 00:20:29'),
(40, 19, 48, 1, 3, 1000000, '-', NULL, NULL, '2023-03-31 03:56:55', '2023-03-31 03:56:55'),
(41, 18, 49, 1, 3, 900000, '-', NULL, NULL, '2023-03-31 04:14:57', '2023-03-31 04:14:57'),
(42, 20, 50, 1, 5, 1200000, '-', NULL, NULL, '2023-03-31 05:40:05', '2023-03-31 05:40:05'),
(43, 18, 33, 1, 2, 1000000, '-', NULL, NULL, '2023-04-18 05:23:02', '2023-04-18 05:23:02'),
(44, 20, 33, 1, 10, 1200000, '-', NULL, NULL, '2023-04-23 03:26:20', '2023-04-23 03:26:20'),
(45, 19, 52, 1, 1, 1000000, 'MODEL JAS HITAM', NULL, NULL, '2023-04-29 05:49:14', '2023-04-29 05:49:14'),
(46, 17, 52, 1, 1, 1000000, 'CUSTOM MODEL', NULL, 'BLAZER PRIA', '2023-04-29 06:01:17', '2023-04-29 06:01:17'),
(47, 22, 53, 1, 15, 100000, 'SERAGAM SD', NULL, NULL, '2023-04-29 06:13:38', '2023-04-29 06:13:38'),
(48, 18, 55, 1, 3, 1000000, '-', NULL, NULL, '2023-05-02 06:31:48', '2023-05-02 06:31:48'),
(51, 19, 58, 1, 2, 1000000, '-', NULL, NULL, '2023-05-04 02:31:00', '2023-05-04 02:31:00'),
(57, 20, 64, 1, 1, 1200000, 'Celana Formal', NULL, NULL, '2023-05-04 07:31:18', '2023-05-04 07:31:18'),
(58, 18, 65, 1, 2, 1000000, '-', NULL, NULL, '2023-05-04 08:09:14', '2023-05-04 08:09:14'),
(59, 18, 66, 1, 3, 1000000, '-', NULL, NULL, '2023-05-04 14:25:43', '2023-05-04 14:25:43');

-- --------------------------------------------------------

--
-- Table structure for table `detail_pemesanan_model_ukuran`
--

CREATE TABLE `detail_pemesanan_model_ukuran` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `detail_pemesanan_model_id` bigint(20) DEFAULT NULL,
  `tinggi_badan` double DEFAULT NULL,
  `berat_badan` double DEFAULT NULL,
  `ukuran_baju` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah_baju_dengan_ukuran_yg_sama` int(11) NOT NULL,
  `panjang_atasan` double DEFAULT NULL,
  `lingkar_dada` double DEFAULT NULL,
  `lingkar_perut_atasan` double DEFAULT NULL,
  `lingkar_pinggul_atasan` double DEFAULT NULL,
  `lebar_bahu` double DEFAULT NULL,
  `panjang_tangan` double DEFAULT NULL,
  `lingkar_siku` double DEFAULT NULL,
  `lingkar_pergelangan` double DEFAULT NULL,
  `kerah` double DEFAULT NULL,
  `ukuran_celana` double DEFAULT NULL,
  `panjang_celana` double DEFAULT NULL,
  `lingkar_perut_celana` double DEFAULT NULL,
  `pesak` double DEFAULT NULL,
  `lingkar_pinggul_celana` double DEFAULT NULL,
  `lingkar_paha` double DEFAULT NULL,
  `lingkar_lutut` double DEFAULT NULL,
  `lingkar_bawah` double DEFAULT NULL,
  `deskripsi_ukuran` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bom_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_pemesanan_model_ukuran`
--

INSERT INTO `detail_pemesanan_model_ukuran` (`id`, `detail_pemesanan_model_id`, `tinggi_badan`, `berat_badan`, `ukuran_baju`, `jumlah_baju_dengan_ukuran_yg_sama`, `panjang_atasan`, `lingkar_dada`, `lingkar_perut_atasan`, `lingkar_pinggul_atasan`, `lebar_bahu`, `panjang_tangan`, `lingkar_siku`, `lingkar_pergelangan`, `kerah`, `ukuran_celana`, `panjang_celana`, `lingkar_perut_celana`, `pesak`, `lingkar_pinggul_celana`, `lingkar_paha`, `lingkar_lutut`, `lingkar_bawah`, `deskripsi_ukuran`, `bom_id`, `created_at`, `updated_at`) VALUES
(27, 31, NULL, NULL, 'M', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2023-03-21 22:30:01', '2023-03-21 22:30:01'),
(28, 31, NULL, NULL, 'L', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2023-03-21 22:42:42', '2023-03-21 22:42:42'),
(29, 35, 180, 70, 'M', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2023-03-23 04:25:29', '2023-03-23 04:40:39'),
(30, 36, NULL, NULL, 'M', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2023-03-23 04:57:03', '2023-03-23 04:57:03'),
(31, 36, NULL, NULL, 'L', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2023-03-23 05:00:01', '2023-03-23 05:00:01'),
(32, 35, NULL, NULL, NULL, 1, 7.5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2023-03-29 05:35:48', '2023-03-29 05:35:48'),
(33, 37, NULL, NULL, 'M', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2023-03-29 22:20:09', '2023-03-29 22:20:09'),
(34, 37, NULL, NULL, 'L', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2023-03-29 22:20:21', '2023-03-29 22:20:21'),
(35, 38, NULL, NULL, 'L', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2023-03-30 10:34:43', '2023-03-30 10:34:43'),
(36, 38, NULL, NULL, 'XL', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2023-03-30 10:34:54', '2023-03-30 10:34:54'),
(37, 39, NULL, NULL, 'M', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2023-03-31 00:23:46', '2023-03-31 00:23:46'),
(38, 39, NULL, NULL, 'L', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2023-03-31 00:24:14', '2023-03-31 00:24:14'),
(39, 40, NULL, NULL, 'M', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2023-03-31 03:57:49', '2023-03-31 03:57:49'),
(40, 41, NULL, NULL, 'S', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2023-03-31 04:16:10', '2023-03-31 04:16:10'),
(41, 42, NULL, NULL, 'L', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2023-03-31 05:41:00', '2023-03-31 05:41:00'),
(42, 45, 180, 60, 'M', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 3, '2023-04-29 05:50:21', '2023-04-29 05:50:21'),
(43, 46, 0, 0, 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 5, '2023-04-29 06:03:07', '2023-04-29 06:03:07'),
(44, 47, 120, 40, 'L', 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 6, '2023-04-29 06:14:16', '2023-04-29 06:14:16'),
(45, 48, NULL, NULL, 'M', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2023-05-02 06:32:58', '2023-05-02 06:32:58'),
(46, 49, NULL, NULL, 'M', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, '2023-05-02 06:42:39', '2023-05-02 06:42:39'),
(47, 50, 100, 40, 'L', 1, 80, 80, 80, 80, 80, 80, 80, 80, 10, 80, 80, 80, 80, 80, 80, 80, 80, NULL, 3, '2023-05-03 05:50:35', '2023-05-03 05:50:35'),
(48, 50, 100, 0, 'L', 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, '0', 3, '2023-05-03 05:56:27', '2023-05-03 05:56:27'),
(49, 50, 100, 0, 'L', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 3, '2023-05-03 05:59:20', '2023-05-03 05:59:20'),
(50, 50, 0, 0, 'L', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 3, '2023-05-03 06:12:05', '2023-05-03 06:12:05'),
(51, 50, 0, 0, 'L', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 3, '2023-05-03 06:13:15', '2023-05-03 06:13:15'),
(52, 50, 0, 0, 'L', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 3, '2023-05-03 06:14:11', '2023-05-03 06:14:11'),
(53, 50, 0, 0, 'L', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 3, '2023-05-03 06:17:05', '2023-05-03 06:17:05'),
(54, 37, NULL, NULL, 'S', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2023-05-04 00:31:48', '2023-05-04 00:31:48'),
(55, 37, NULL, NULL, 'S', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2023-05-04 00:44:10', '2023-05-04 00:44:10'),
(56, 51, NULL, NULL, 'L', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, '2023-05-04 02:32:07', '2023-05-04 02:32:07'),
(57, 52, NULL, NULL, 'L', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, '2023-05-04 02:37:04', '2023-05-04 02:37:04'),
(58, 53, NULL, NULL, 'XL', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2023-05-04 05:52:17', '2023-05-04 05:52:17'),
(59, 54, NULL, NULL, 'XL', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2023-05-04 05:54:51', '2023-05-04 05:54:51'),
(60, 55, NULL, NULL, 'XL', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2023-05-04 06:06:30', '2023-05-04 06:06:30'),
(61, 56, NULL, NULL, 'XL', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, '2023-05-04 06:28:00', '2023-05-04 06:28:00'),
(62, 57, 180, 0, 'S', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '0', 1, '2023-05-04 07:31:55', '2023-05-04 07:31:55'),
(63, 57, 0, 0, 'S', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, '2023-05-04 07:51:35', '2023-05-04 07:51:35'),
(64, 57, 0, 0, 'XL', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, '2023-05-04 07:55:22', '2023-05-04 07:55:22'),
(65, 58, NULL, NULL, 'L', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, '2023-05-04 08:10:05', '2023-05-04 08:10:05'),
(66, 59, NULL, NULL, 'XL', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, '2023-05-04 14:26:01', '2023-05-04 14:26:01');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jasa_ekspedisi`
--

CREATE TABLE `jasa_ekspedisi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `jasa_ekspedisi` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jasa_ekspedisi`
--

INSERT INTO `jasa_ekspedisi` (`id`, `jasa_ekspedisi`, `created_at`, `updated_at`) VALUES
(2, 'JNT', '2023-03-30 17:48:27', '2023-03-30 17:48:27'),
(3, 'JNE', '2023-03-30 17:48:32', '2023-03-30 17:48:32'),
(4, 'Sicepat', '2023-03-30 17:48:37', '2023-03-30 17:48:37');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_model`
--

CREATE TABLE `jenis_model` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_jenismodel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jenis_model`
--

INSERT INTO `jenis_model` (`id`, `nama_jenismodel`, `created_at`, `updated_at`) VALUES
(1, 'Unisex', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kolom_rak`
--

CREATE TABLE `kolom_rak` (
  `id` bigint(20) NOT NULL,
  `nama_rak` varchar(255) NOT NULL,
  `nama_kolom` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kolom_rak`
--

INSERT INTO `kolom_rak` (`id`, `nama_rak`, `nama_kolom`, `created_at`, `updated_at`) VALUES
(12, 'Rak 1', 'Col no 1', '2023-03-21 06:06:28', '2023-03-21 06:06:28'),
(13, 'Rak 1', 'Col no 2', '2023-03-21 06:06:34', '2023-03-21 06:06:34'),
(15, 'Rak 1', 'Col no 3', '2023-03-21 06:07:12', '2023-03-21 06:07:12'),
(16, 'Rak 2', 'Col no 1', '2023-03-21 06:07:25', '2023-03-21 06:07:25'),
(17, 'Rak 2', 'Col no 2', '2023-03-21 06:07:32', '2023-03-21 06:07:32'),
(18, 'Rak 2', 'Col no 3', '2023-03-21 13:07:47', '2023-03-21 06:07:47'),
(20, 'Rak 3', 'Col no 1', '2023-03-31 00:15:03', '2023-03-31 00:15:03');

-- --------------------------------------------------------

--
-- Table structure for table `letak_bahan_baku`
--

CREATE TABLE `letak_bahan_baku` (
  `nama_letak` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_09_20_125807_create_suppliers_table', 1),
(6, '2022_09_20_132010_create_bahan_bakus_table', 1),
(7, '2022_09_20_135502_create_penjahits_table', 1),
(8, '2022_09_20_141725_create_pelanggans_table', 1),
(9, '2022_09_21_122513_create_detail_pemesanan_bahanbaku', 1),
(10, '2022_09_21_122749_create_pembelian_bahanbaku', 1),
(11, '2022_09_21_124553_create_detail_pemesanan_model', 1),
(12, '2022_09_21_124631_create_jenis_model', 1),
(13, '2022_09_21_124717_create_model', 1),
(14, '2022_09_21_124907_create_pemesanan', 1),
(15, '2022_09_21_125852_create_realisasi_produksi', 1),
(16, '2022_09_21_130238_create_perencanaan_produksi', 1),
(17, '2022_09_21_130530_create_pengambilan', 1),
(18, '2022_09_21_130934_create_retur_pemesanan', 1),
(19, '2022_09_21_131240_create_realisasi_penjahit', 1),
(20, '2022_09_21_131318_create_proses_produksi', 1),
(21, '2022_10_05_095003_create_detail_pembelian_bahanbaku', 1),
(24, '2022_10_13_112851_add_detail_pemesanan_model_id_to_perencanaan_produksi_table', 2),
(25, '2022_10_15_092236_add_detail_pemesanan_model_id_to_detail_pemesanan_bahanbaku', 3);

-- --------------------------------------------------------

--
-- Table structure for table `model`
--

CREATE TABLE `model` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `jenis_model` bigint(20) DEFAULT NULL,
  `pelanggan_id` bigint(20) DEFAULT NULL,
  `foto_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ongkos_jahit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model`
--

INSERT INTO `model` (`id`, `jenis_model`, `pelanggan_id`, `foto_model`, `nama_model`, `ongkos_jahit`, `deskripsi_model`, `created_at`, `updated_at`) VALUES
(17, 1, NULL, NULL, 'model pelanggan', NULL, NULL, '2023-03-21 22:00:37', '2023-03-28 03:01:54'),
(18, 1, NULL, '20230322050130.png', 'JAS PENGANTIN PRIA PUTIH', '1000000', NULL, '2023-03-21 22:01:08', '2023-03-21 22:01:30'),
(19, 1, NULL, '20230322050439.png', 'JAS PENGANTIN PRIA HITAM', '1000000', NULL, '2023-03-21 22:04:39', '2023-03-21 22:04:39'),
(20, 1, NULL, '20230331000529.png', 'Jas dan Celana Formal Putih', '1200000', NULL, '2023-03-30 17:05:29', '2023-03-30 17:05:29'),
(21, 1, NULL, '20230331073713.png', 'Jas dan Celana Formal Hitam', '1200000', NULL, '2023-03-31 00:37:13', '2023-03-31 00:37:13');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_pelanggan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_telepon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id`, `nama_pelanggan`, `email`, `no_telepon`, `alamat`, `created_at`, `updated_at`) VALUES
(15, 'Rafli', 'rafly@gmail.com', '08113111862', NULL, '2023-03-21 22:12:51', '2023-03-28 19:05:11'),
(16, 'aldo', 'aldo@gmail.com', '08512345678', 'wiyung', '2023-03-21 22:14:25', '2023-03-21 22:14:25'),
(17, 'farrell', 'farrell@gmail.com', '08512345678', 'surabaya', '2023-03-30 17:06:09', '2023-03-30 17:06:09'),
(18, 'andi', 'andi@gmail.com', '08198765432', 'jl jojoran', '2023-03-30 17:16:13', '2023-03-30 17:16:13'),
(19, 'azhar', 'azhar@gmail.com', '0851234678', 'surabaya', '2023-03-30 17:17:36', '2023-03-30 17:17:36'),
(20, 'dani', 'danimaulanaazhar@gmail.com', '08113111862', 'surabaya', '2023-03-31 00:38:05', '2023-03-31 00:38:05'),
(22, 'krisna', 'krisna@gmail.com', '081234567777', 'jl karah', '2023-05-02 07:27:15', '2023-05-02 07:27:15'),
(23, 'Dian', 'dian@gmail.com', '08123456789', 'surabaya', '2023-05-04 08:29:51', '2023-05-04 08:29:51');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_bahanbaku`
--

CREATE TABLE `pembelian_bahanbaku` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `supplier_id` bigint(20) DEFAULT NULL,
  `penjahit_id` bigint(20) DEFAULT NULL,
  `tanggal_beli` date DEFAULT NULL,
  `bayar` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pembelian_bahanbaku`
--

INSERT INTO `pembelian_bahanbaku` (`id`, `supplier_id`, `penjahit_id`, `tanggal_beli`, `bayar`, `total`, `created_at`, `updated_at`) VALUES
(21, 6, 1, '2023-03-21', 8415000, 8415000, '2023-03-21 06:33:38', '2023-03-21 07:01:13'),
(22, 9, 1, '2023-03-28', 5000000, 5650000, '2023-03-28 02:51:36', '2023-03-28 21:05:05'),
(29, 11, 1, '2023-03-13', 2500000, 2500000, '2023-03-30 16:15:07', '2023-03-30 16:15:44'),
(30, 13, 1, '2023-03-14', 3825000, 3825000, '2023-03-30 16:21:31', '2023-03-30 16:22:42'),
(31, 6, 1, '2023-03-20', 900000, 900000, '2023-03-30 16:35:13', '2023-03-30 16:36:49'),
(33, 14, 1, '2023-03-31', 1800000, 1800000, '2023-03-31 00:27:42', '2023-03-31 00:35:36'),
(34, 14, 1, '2023-03-31', NULL, NULL, '2023-03-31 00:36:06', '2023-03-31 00:36:06');

-- --------------------------------------------------------

--
-- Table structure for table `pemesanan`
--

CREATE TABLE `pemesanan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pelanggan_id` bigint(20) DEFAULT NULL,
  `penjahit_id` bigint(20) DEFAULT NULL,
  `proses_produksi_id` bigint(20) DEFAULT NULL,
  `pengambilan_id` bigint(20) DEFAULT NULL,
  `perencanaan_produksi_id` bigint(20) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `total_ongkos` double DEFAULT NULL,
  `bayar` double DEFAULT NULL,
  `status_pembayaran` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Lunas, Bayar Sebagian',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pemesanan`
--

INSERT INTO `pemesanan` (`id`, `pelanggan_id`, `penjahit_id`, `proses_produksi_id`, `pengambilan_id`, `perencanaan_produksi_id`, `tanggal`, `total_ongkos`, `bayar`, `status_pembayaran`, `created_at`, `updated_at`) VALUES
(33, 15, 1, NULL, NULL, NULL, '2023-04-05', 10000000, 10000000, 'Lunas', '2023-03-21 22:15:06', '2023-03-23 06:07:07'),
(45, 15, 1, NULL, NULL, NULL, '2023-05-02', 5000000, 5000000, 'Lunas', '2023-03-29 22:19:16', '2023-03-29 22:19:56'),
(46, 16, 1, NULL, NULL, NULL, '2023-04-25', 5000000, 2500000, 'Bayar Sebagian', '2023-03-30 10:33:02', '2023-03-30 10:34:30'),
(47, 17, 1, NULL, NULL, NULL, '2023-04-12', 5000000, 5000000, 'Lunas', '2023-03-31 00:18:59', '2023-03-31 00:49:21'),
(48, 18, 1, NULL, NULL, NULL, '2023-04-17', 3000000, 3000000, 'Lunas', '2023-03-31 03:56:42', '2023-03-31 04:06:49'),
(49, 18, 1, NULL, NULL, NULL, '2023-04-17', 2700000, 2700000, 'Lunas', '2023-03-31 04:14:42', '2023-03-31 04:15:41'),
(50, 19, 1, NULL, NULL, NULL, '2023-04-21', 6000000, 3000000, 'Bayar Sebagian', '2023-03-31 05:39:50', '2023-03-31 05:40:35'),
(52, 20, 1, NULL, NULL, NULL, '2023-05-06', 2000000, 2000000, 'Lunas', '2023-04-29 05:48:51', '2023-05-04 02:57:27'),
(53, 21, 1, NULL, NULL, NULL, '2023-05-29', NULL, NULL, NULL, '2023-04-29 06:12:51', '2023-04-29 06:12:51'),
(55, 22, 1, NULL, NULL, NULL, '2023-05-05', 3000000, 1500000, 'Bayar Sebagian', '2023-05-02 06:29:07', '2023-05-02 06:32:30'),
(58, 22, 1, NULL, NULL, NULL, '2023-05-18', 2000000, 1000000, 'Bayar Sebagian', '2023-05-04 02:29:43', '2023-05-04 02:32:33'),
(64, 23, 1, NULL, NULL, NULL, '2023-05-31', NULL, NULL, NULL, '2023-05-04 07:30:05', '2023-05-04 07:30:05'),
(65, 15, 1, NULL, NULL, NULL, '2023-05-25', NULL, NULL, NULL, '2023-05-04 08:08:52', '2023-05-04 08:08:52'),
(66, 15, 1, NULL, NULL, NULL, '2023-05-31', NULL, NULL, NULL, '2023-05-04 14:24:29', '2023-05-04 14:24:29'),
(67, 23, 1, NULL, NULL, NULL, '2023-05-29', NULL, NULL, NULL, '2023-05-08 03:00:25', '2023-05-08 03:00:25');

-- --------------------------------------------------------

--
-- Table structure for table `pengambilan`
--

CREATE TABLE `pengambilan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pemesanan_id` bigint(20) NOT NULL,
  `jasa_ekspedisi_id` bigint(20) DEFAULT NULL,
  `opsi_pengambilan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `alamat_pengiriman` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biaya_pengiriman` double DEFAULT NULL,
  `no_resi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengambilan`
--

INSERT INTO `pengambilan` (`id`, `pemesanan_id`, `jasa_ekspedisi_id`, `opsi_pengambilan`, `tanggal`, `alamat_pengiriman`, `biaya_pengiriman`, `no_resi`, `created_at`, `updated_at`) VALUES
(7, 33, NULL, 'Ambil', '2023-04-05', NULL, NULL, NULL, '2023-03-22 23:01:00', '2023-03-22 23:01:00'),
(8, 47, NULL, 'Ambil', '2023-04-13', NULL, NULL, NULL, '2023-03-30 17:50:34', '2023-03-30 17:50:34'),
(9, 45, 2, 'Kirim', '2023-03-27', 'Jl. Raya Juanda Sidoarjo', 10000, '12345678', '2023-03-30 17:51:40', '2023-03-30 17:51:40'),
(10, 48, NULL, 'Ambil', '2023-04-17', NULL, NULL, NULL, '2023-03-30 21:07:15', '2023-03-30 21:07:15');

-- --------------------------------------------------------

--
-- Table structure for table `penjahit`
--

CREATE TABLE `penjahit` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_telepon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_penjahit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `penjahit`
--

INSERT INTO `penjahit` (`id`, `email`, `no_telepon`, `nama_penjahit`, `password`, `created_at`, `updated_at`) VALUES
(1, 'maul@gmail.com', '08213123', 'maul', NULL, NULL, NULL),
(2, 'andi@gmail.com', '086445678', 'Andi', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `perencanaan_produksi`
--

CREATE TABLE `perencanaan_produksi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `proses_produksi_id` bigint(20) DEFAULT NULL,
  `detail_pemesanan_model_id` bigint(20) DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `gambar_pola` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kepala_penjahit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `perencanaan_produksi`
--

INSERT INTO `perencanaan_produksi` (`id`, `proses_produksi_id`, `detail_pemesanan_model_id`, `tanggal_mulai`, `tanggal_selesai`, `gambar_pola`, `kepala_penjahit`, `created_at`, `updated_at`, `user_id`) VALUES
(20, 13, 31, '2023-03-22', '2023-03-22', NULL, NULL, '2023-03-21 22:59:50', '2023-03-21 22:59:50', 3),
(22, 17, 31, '2023-04-04', '2023-04-05', NULL, NULL, '2023-03-22 19:30:36', '2023-03-22 19:30:36', 6),
(24, 15, 31, '2023-03-24', '2023-04-03', NULL, NULL, '2023-03-22 22:52:47', '2023-03-22 22:52:47', 3),
(31, 13, 37, '2023-03-30', '2023-03-30', NULL, NULL, '2023-03-29 15:21:10', '2023-03-29 15:21:10', 3),
(32, 17, 37, '2023-04-25', '2023-04-27', NULL, NULL, '2023-03-29 15:21:40', '2023-03-29 22:10:25', 5),
(34, 15, 37, '2023-03-31', '2023-04-24', NULL, NULL, '2023-03-29 22:09:08', '2023-03-29 22:09:08', 3),
(35, 13, 38, '2023-03-30', '2023-03-30', NULL, NULL, '2023-03-30 03:35:43', '2023-03-30 03:35:43', 3),
(36, 15, 38, '2023-03-31', '2023-04-17', NULL, NULL, '2023-03-30 03:36:29', '2023-03-30 03:36:29', 3),
(37, 17, 38, '2023-04-18', '2023-04-20', NULL, NULL, '2023-03-30 03:37:07', '2023-03-30 03:37:07', 9),
(38, 13, 39, '2023-03-31', '2023-03-31', NULL, NULL, '2023-03-30 17:26:33', '2023-03-30 17:26:33', 3),
(39, 15, 39, '2023-04-03', '2023-04-10', NULL, NULL, '2023-03-30 17:27:37', '2023-03-30 17:27:37', 5),
(40, 17, 39, '2023-04-11', '2023-04-12', NULL, NULL, '2023-03-30 17:28:26', '2023-03-30 17:28:26', 6),
(41, 13, 40, '2023-03-31', '2023-03-31', NULL, NULL, '2023-03-30 20:58:57', '2023-03-30 20:58:57', 3),
(42, 15, 40, '2023-04-03', '2023-03-15', NULL, NULL, '2023-03-30 20:59:22', '2023-03-30 20:59:22', 6),
(43, 17, 40, '2023-04-12', '2023-04-14', NULL, NULL, '2023-03-30 20:59:49', '2023-03-30 20:59:49', 5),
(44, 13, 41, '2023-03-31', '2023-03-31', NULL, NULL, '2023-03-30 21:16:43', '2023-03-30 21:16:43', 3),
(45, 15, 41, '2023-04-03', '2023-04-12', NULL, NULL, '2023-03-30 21:17:07', '2023-03-30 21:17:07', 6),
(46, 17, 41, '2023-04-13', '2023-04-14', NULL, NULL, '2023-03-30 21:17:32', '2023-03-30 21:17:32', 5),
(47, 13, 48, '2023-05-02', '2023-05-02', NULL, NULL, '2023-05-02 08:05:33', '2023-05-02 08:05:33', 3),
(48, 15, 48, '2023-05-03', '2023-05-04', NULL, NULL, '2023-05-02 08:05:58', '2023-05-02 08:05:58', 3),
(49, 17, 48, '2023-05-05', '2023-05-05', NULL, NULL, '2023-05-02 08:06:39', '2023-05-02 08:06:39', 3);

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `proses_produksi`
--

CREATE TABLE `proses_produksi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_prosesproduksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `proses_produksi`
--

INSERT INTO `proses_produksi` (`id`, `nama_prosesproduksi`, `created_at`, `updated_at`) VALUES
(13, 'Pemotongan Kain', '2023-03-21 22:55:18', '2023-03-21 22:55:18'),
(15, 'Jahit Pakaian Atasan', '2023-03-21 22:56:18', '2023-03-21 22:56:18'),
(16, 'Jahit Celana', '2023-03-21 22:56:28', '2023-03-21 22:56:28'),
(17, 'Finishing', '2023-03-21 22:56:34', '2023-03-21 22:56:34');

-- --------------------------------------------------------

--
-- Table structure for table `realisasi_penjahit`
--

CREATE TABLE `realisasi_penjahit` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `realisasi_produksi_id` bigint(20) DEFAULT NULL,
  `penjahit_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `realisasi_produksi`
--

CREATE TABLE `realisasi_produksi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pemesanan_id` bigint(20) DEFAULT NULL,
  `perencanaan_produksi_id` bigint(20) DEFAULT NULL,
  `proses_produksi_id` bigint(20) DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `realisasi_produksi`
--

INSERT INTO `realisasi_produksi` (`id`, `pemesanan_id`, `perencanaan_produksi_id`, `proses_produksi_id`, `tanggal_mulai`, `tanggal_selesai`, `foto`, `keterangan`, `created_at`, `updated_at`) VALUES
(21, 45, 31, 13, '2023-03-30', '2023-03-30', NULL, NULL, '2023-03-29 15:23:14', '2023-03-29 15:23:14'),
(23, 33, 20, 13, '2023-03-22', '2023-03-22', NULL, NULL, '2023-03-29 15:54:44', '2023-03-29 15:54:44'),
(24, 33, 24, 15, '2023-03-23', '2023-04-03', NULL, NULL, '2023-03-29 15:55:30', '2023-03-29 15:55:30'),
(25, 33, 22, 17, '2023-04-04', '2023-04-05', NULL, NULL, '2023-03-29 15:57:19', '2023-03-29 15:57:19'),
(26, 45, 34, 15, '2023-03-31', '2023-04-25', NULL, NULL, '2023-03-29 22:15:54', '2023-03-29 22:15:54'),
(27, 46, 35, 13, '2023-03-30', '2023-03-31', NULL, NULL, '2023-03-30 03:38:01', '2023-03-30 03:38:01'),
(28, 46, 36, 15, '2023-04-01', '2023-04-14', NULL, NULL, '2023-03-30 03:38:33', '2023-03-30 03:38:33'),
(29, 47, 38, 13, '2023-03-31', '2023-03-31', NULL, NULL, '2023-03-30 17:32:29', '2023-03-30 17:32:29'),
(30, 47, 39, 15, '2023-04-01', '2023-04-10', NULL, NULL, '2023-03-30 17:33:09', '2023-03-30 17:33:09'),
(31, 47, 40, 17, '2023-04-11', '2023-04-12', NULL, NULL, '2023-03-30 17:34:32', '2023-03-30 17:34:32'),
(32, 48, 41, 13, '2023-03-31', '2023-03-31', NULL, NULL, '2023-03-30 21:01:40', '2023-03-30 21:01:40'),
(33, 48, 42, 15, '2023-04-03', '2023-04-11', NULL, NULL, '2023-03-30 21:02:11', '2023-03-30 21:02:11'),
(34, 48, 43, 17, '2023-04-12', '2023-04-14', NULL, NULL, '2023-03-30 21:02:47', '2023-03-30 21:02:47'),
(35, 49, 44, 13, '2023-03-31', '2023-03-31', NULL, NULL, '2023-03-30 21:19:13', '2023-03-30 21:19:13'),
(36, 55, 47, 13, '2023-05-02', '2023-05-02', NULL, NULL, '2023-05-02 08:09:21', '2023-05-02 08:09:21'),
(37, 46, 37, 17, '2023-04-15', '2023-04-15', NULL, NULL, '2023-05-17 05:10:30', '2023-05-17 05:10:30');

-- --------------------------------------------------------

--
-- Table structure for table `retur_pemesanan`
--

CREATE TABLE `retur_pemesanan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pemesanan_id` bigint(20) DEFAULT NULL,
  `tanggal_pengajuan` date DEFAULT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_supplier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_telepon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `nama_supplier`, `alamat`, `email`, `nomor_telepon`, `created_at`, `updated_at`) VALUES
(6, 'Toko Alpha', 'Jl Kembang Jepun', 'tokoalpha@gmail.com', '0812345678', '2023-03-21 06:08:12', '2023-03-21 06:08:12'),
(9, 'Toko Beta', 'pasar atum', 'tokobeta@gmail.com', '0851234567', '2023-03-21 06:11:58', '2023-03-21 06:11:58'),
(11, 'toko carli', 'sidoarjo', 'tokocarli@gmail.com', '08127484885', '2023-03-21 06:12:37', '2023-03-21 06:12:37'),
(13, 'Toko Delta', 'pasar atum', 'tokodelta@gmail.com', '0821345678', '2023-03-30 15:59:41', '2023-03-30 16:00:15'),
(14, 'Toko Eka', 'surabaya', 'tokoeka@gmail.com', '0851234587', '2023-03-31 00:22:06', '2023-03-31 00:22:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previledge` enum('Penjahit','Pemilik','Kepala','Admin','Finishing') COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_penjahit` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `previledge`, `id_penjahit`, `created_at`, `updated_at`) VALUES
(1, 'maul', 'maul@gmail.com', NULL, '$2y$10$CkaE7WZar2jLhrS2CIrBKOpEnfIfwxUOC4x//g0CNZwtsEQZ20nGa', NULL, 'Admin', 1, '2022-10-10 10:20:19', '2022-10-10 10:20:19'),
(2, 'atam', 'atam@gmail.com', NULL, '$2y$10$CkaE7WZar2jLhrS2CIrBKOpEnfIfwxUOC4x//g0CNZwtsEQZ20nGa', NULL, 'Pemilik', 1, '2022-10-10 10:20:19', '2022-10-10 10:20:19'),
(3, 'lana', 'lana@gmail.com', NULL, '$2y$10$CkaE7WZar2jLhrS2CIrBKOpEnfIfwxUOC4x//g0CNZwtsEQZ20nGa', NULL, 'Kepala', 1, '2022-10-10 10:20:19', '2022-10-10 10:20:19'),
(4, 'agung', 'agung@gmail.com', NULL, '$2y$10$CkaE7WZar2jLhrS2CIrBKOpEnfIfwxUOC4x//g0CNZwtsEQZ20nGa', NULL, 'Penjahit', 1, '2022-10-10 10:20:19', '2022-10-10 10:20:19'),
(5, 'mus', 'mus@gmail.com', NULL, '$2y$10$CkaE7WZar2jLhrS2CIrBKOpEnfIfwxUOC4x//g0CNZwtsEQZ20nGa', NULL, 'Finishing', 1, '2022-10-10 10:20:19', '2022-10-10 10:20:19'),
(6, 'aben', 'aben@gmail.com', NULL, '$2y$10$vJZsR2S/szpx5HUWryMq2uiLSeQi0PWd9UQqni42B33cDaieVgD8q', NULL, 'Kepala', 1, '2023-02-24 07:53:07', '2023-02-24 07:53:07'),
(9, 'john', 'john@gmail.com', NULL, '$2y$10$0UbTjOsTOw9.YCYvVXVLlOu0JPh1VO.d4afSMlHU/dqy/E7Jsn2Hi', NULL, 'Kepala', 2, '2023-03-06 18:27:58', '2023-03-06 18:27:58');

-- --------------------------------------------------------

--
-- Table structure for table `users_old`
--

CREATE TABLE `users_old` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previledge` enum('Penjahit','Pemilik','Kepala','Admin','Finishing') COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_penjahit` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_old`
--

INSERT INTO `users_old` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `previledge`, `id_penjahit`, `created_at`, `updated_at`) VALUES
(1, 'maul', 'admin@admin.com', NULL, '$2y$10$CkaE7WZar2jLhrS2CIrBKOpEnfIfwxUOC4x//g0CNZwtsEQZ20nGa', NULL, 'Admin', 1, '2022-10-10 10:20:19', '2022-10-10 10:20:19'),
(2, 'atam', 'pemilik@pemilik.com', NULL, '$2y$10$CkaE7WZar2jLhrS2CIrBKOpEnfIfwxUOC4x//g0CNZwtsEQZ20nGa', NULL, 'Pemilik', 1, '2022-10-10 10:20:19', '2022-10-10 10:20:19'),
(3, 'lana', 'kepala@kepala.com', NULL, '$2y$10$CkaE7WZar2jLhrS2CIrBKOpEnfIfwxUOC4x//g0CNZwtsEQZ20nGa', NULL, 'Kepala', 1, '2022-10-10 10:20:19', '2022-10-10 10:20:19'),
(4, 'agung', 'penjahit@penjahit.com', NULL, '$2y$10$CkaE7WZar2jLhrS2CIrBKOpEnfIfwxUOC4x//g0CNZwtsEQZ20nGa', NULL, 'Penjahit', 1, '2022-10-10 10:20:19', '2022-10-10 10:20:19'),
(5, 'mus', 'mus@gmail.com', NULL, '$2y$10$CkaE7WZar2jLhrS2CIrBKOpEnfIfwxUOC4x//g0CNZwtsEQZ20nGa', NULL, 'Finishing', 1, '2022-10-10 10:20:19', '2022-10-10 10:20:19'),
(6, 'aben', 'aben@gmail.com', NULL, '$2y$10$vJZsR2S/szpx5HUWryMq2uiLSeQi0PWd9UQqni42B33cDaieVgD8q', NULL, 'Kepala', 1, '2023-02-24 07:53:07', '2023-02-24 07:53:07'),
(9, 'john', 'john@gmail.com', NULL, '$2y$10$0UbTjOsTOw9.YCYvVXVLlOu0JPh1VO.d4afSMlHU/dqy/E7Jsn2Hi', NULL, 'Kepala', 2, '2023-03-06 18:27:58', '2023-03-06 18:27:58');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_deadline_pesanan`
-- (See below for the actual view)
--
CREATE TABLE `view_deadline_pesanan` (
`nama_pelanggan` varchar(255)
,`estimasi_selesai_hmin5` date
,`nama_model` varchar(255)
,`nama_model_detail` varchar(100)
,`jumlah` double
,`selisih_hari` int(7)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_laporan_daftar_tanggungan_produksi_jahit`
-- (See below for the actual view)
--
CREATE TABLE `view_laporan_daftar_tanggungan_produksi_jahit` (
`id` bigint(20) unsigned
,`penjahit_id` bigint(20)
,`nama_pelanggan` varchar(255)
,`nama_model` varchar(255)
,`tanggal_selesai` date
,`nama_model_detail` varchar(100)
,`jumlah` double
,`id_proses_produksi` bigint(20) unsigned
,`nama_prosesproduksi` varchar(255)
,`realisasi_tanggal_selesai` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_laporan_daftar_tanggungan_produksi_jahit_group`
-- (See below for the actual view)
--
CREATE TABLE `view_laporan_daftar_tanggungan_produksi_jahit_group` (
`id` bigint(20) unsigned
,`penjahit_id` bigint(20)
,`nama_pelanggan` varchar(255)
,`nama_model` varchar(255)
,`tanggal_selesai` date
,`nama_model_detail` varchar(100)
,`jumlah` double
,`id_proses_produksi` bigint(20) unsigned
,`realisasi_tanggal_selesai` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_laporan_daftar_tanggungan_produksi_jahit_group2`
-- (See below for the actual view)
--
CREATE TABLE `view_laporan_daftar_tanggungan_produksi_jahit_group2` (
`id` bigint(20) unsigned
,`penjahit_id` bigint(20)
,`nama_pelanggan` varchar(255)
,`nama_model` varchar(255)
,`tanggal_selesai` date
,`nama_model_detail` varchar(100)
,`jumlah` double
,`id_proses_produksi` bigint(20) unsigned
,`realisasi_tanggal_selesai` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_pemesanan_belum_finish`
-- (See below for the actual view)
--
CREATE TABLE `view_pemesanan_belum_finish` (
`id` bigint(20) unsigned
,`penjahit_id` bigint(20)
,`nama_pelanggan` varchar(255)
,`nama_model` varchar(255)
,`tanggal_selesai` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_tanggungan_pesanan`
-- (See below for the actual view)
--
CREATE TABLE `view_tanggungan_pesanan` (
`id` bigint(20) unsigned
,`penjahit_id` bigint(20)
,`nama_pelanggan` varchar(255)
,`nama_model` varchar(255)
,`tanggal_selesai` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_transaksi_pemesanan_model`
-- (See below for the actual view)
--
CREATE TABLE `view_transaksi_pemesanan_model` (
`id_detail_pemesanan_model` bigint(20) unsigned
,`tanggal` date
,`pelanggan_id` bigint(20)
,`nama_pelanggan` varchar(255)
,`penjahit_id` bigint(20)
,`nama_penjahit` varchar(255)
,`model_id` bigint(20)
,`nama_model` varchar(255)
,`jenis_model` varchar(255)
,`jumlah` double
,`satuan` varchar(3)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_ukuran`
-- (See below for the actual view)
--
CREATE TABLE `view_ukuran` (
`id_pemesanan` bigint(20) unsigned
,`id_pemesanan_model` bigint(20) unsigned
,`id_ukuran` bigint(20) unsigned
,`nama_pelanggan` varchar(255)
,`tanggal` date
,`nama_model` varchar(255)
,`ukuran_baju` varchar(255)
,`deskripsi_ukuran` text
);

-- --------------------------------------------------------

--
-- Structure for view `view_deadline_pesanan`
--
DROP TABLE IF EXISTS `view_deadline_pesanan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_deadline_pesanan`  AS SELECT `view_laporan_daftar_tanggungan_produksi_jahit_group2`.`nama_pelanggan` AS `nama_pelanggan`, `view_laporan_daftar_tanggungan_produksi_jahit_group2`.`tanggal_selesai` AS `estimasi_selesai_hmin5`, `view_laporan_daftar_tanggungan_produksi_jahit_group2`.`nama_model` AS `nama_model`, `view_laporan_daftar_tanggungan_produksi_jahit_group2`.`nama_model_detail` AS `nama_model_detail`, `view_laporan_daftar_tanggungan_produksi_jahit_group2`.`jumlah` AS `jumlah`, to_days(`view_laporan_daftar_tanggungan_produksi_jahit_group2`.`tanggal_selesai`) - to_days(curdate()) AS `selisih_hari` FROM `view_laporan_daftar_tanggungan_produksi_jahit_group2``view_laporan_daftar_tanggungan_produksi_jahit_group2`  ;

-- --------------------------------------------------------

--
-- Structure for view `view_laporan_daftar_tanggungan_produksi_jahit`
--
DROP TABLE IF EXISTS `view_laporan_daftar_tanggungan_produksi_jahit`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_laporan_daftar_tanggungan_produksi_jahit`  AS   (select `pemesanan`.`id` AS `id`,`pemesanan`.`penjahit_id` AS `penjahit_id`,`pelanggan`.`nama_pelanggan` AS `nama_pelanggan`,`model`.`nama_model` AS `nama_model`,`pemesanan`.`tanggal` AS `tanggal_selesai`,`detail_pemesanan_model`.`nama_model_detail` AS `nama_model_detail`,`detail_pemesanan_model`.`banyaknya` AS `jumlah`,`proses_produksi`.`id` AS `id_proses_produksi`,`proses_produksi`.`nama_prosesproduksi` AS `nama_prosesproduksi`,`realisasi_produksi`.`tanggal_selesai` AS `realisasi_tanggal_selesai` from ((((((`pemesanan` join `pelanggan` on(`pemesanan`.`pelanggan_id` = `pelanggan`.`id`)) join `detail_pemesanan_model` on(`pemesanan`.`id` = `detail_pemesanan_model`.`pemesanan_id`)) join `model` on(`detail_pemesanan_model`.`model_id` = `model`.`id`)) join `perencanaan_produksi` on(`detail_pemesanan_model`.`id` = `perencanaan_produksi`.`detail_pemesanan_model_id`)) join `realisasi_produksi` on(`perencanaan_produksi`.`id` = `realisasi_produksi`.`perencanaan_produksi_id`)) join `proses_produksi` on(`realisasi_produksi`.`proses_produksi_id` = `proses_produksi`.`id`)) where `proses_produksi`.`nama_prosesproduksi` <> 'Finishing' order by `realisasi_produksi`.`tanggal_selesai`)  ;

-- --------------------------------------------------------

--
-- Structure for view `view_laporan_daftar_tanggungan_produksi_jahit_group`
--
DROP TABLE IF EXISTS `view_laporan_daftar_tanggungan_produksi_jahit_group`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_laporan_daftar_tanggungan_produksi_jahit_group`  AS   (select `view_laporan_daftar_tanggungan_produksi_jahit`.`id` AS `id`,`view_laporan_daftar_tanggungan_produksi_jahit`.`penjahit_id` AS `penjahit_id`,`view_laporan_daftar_tanggungan_produksi_jahit`.`nama_pelanggan` AS `nama_pelanggan`,`view_laporan_daftar_tanggungan_produksi_jahit`.`nama_model` AS `nama_model`,`view_laporan_daftar_tanggungan_produksi_jahit`.`tanggal_selesai` AS `tanggal_selesai`,`view_laporan_daftar_tanggungan_produksi_jahit`.`nama_model_detail` AS `nama_model_detail`,`view_laporan_daftar_tanggungan_produksi_jahit`.`jumlah` AS `jumlah`,max(`view_laporan_daftar_tanggungan_produksi_jahit`.`id_proses_produksi`) AS `id_proses_produksi`,`view_laporan_daftar_tanggungan_produksi_jahit`.`realisasi_tanggal_selesai` AS `realisasi_tanggal_selesai` from `view_laporan_daftar_tanggungan_produksi_jahit` group by `view_laporan_daftar_tanggungan_produksi_jahit`.`id`)  ;

-- --------------------------------------------------------

--
-- Structure for view `view_laporan_daftar_tanggungan_produksi_jahit_group2`
--
DROP TABLE IF EXISTS `view_laporan_daftar_tanggungan_produksi_jahit_group2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_laporan_daftar_tanggungan_produksi_jahit_group2`  AS   (select `view_laporan_daftar_tanggungan_produksi_jahit`.`id` AS `id`,`view_laporan_daftar_tanggungan_produksi_jahit`.`penjahit_id` AS `penjahit_id`,`view_laporan_daftar_tanggungan_produksi_jahit`.`nama_pelanggan` AS `nama_pelanggan`,`view_laporan_daftar_tanggungan_produksi_jahit`.`nama_model` AS `nama_model`,`view_laporan_daftar_tanggungan_produksi_jahit`.`tanggal_selesai` AS `tanggal_selesai`,`view_laporan_daftar_tanggungan_produksi_jahit`.`nama_model_detail` AS `nama_model_detail`,`view_laporan_daftar_tanggungan_produksi_jahit`.`jumlah` AS `jumlah`,max(`view_laporan_daftar_tanggungan_produksi_jahit`.`id_proses_produksi`) AS `id_proses_produksi`,max(`view_laporan_daftar_tanggungan_produksi_jahit`.`realisasi_tanggal_selesai`) AS `realisasi_tanggal_selesai` from `view_laporan_daftar_tanggungan_produksi_jahit` group by `view_laporan_daftar_tanggungan_produksi_jahit`.`id`)  ;

-- --------------------------------------------------------

--
-- Structure for view `view_pemesanan_belum_finish`
--
DROP TABLE IF EXISTS `view_pemesanan_belum_finish`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_pemesanan_belum_finish`  AS   (select `pemesanan`.`id` AS `id`,`pemesanan`.`penjahit_id` AS `penjahit_id`,`pelanggan`.`nama_pelanggan` AS `nama_pelanggan`,`model`.`nama_model` AS `nama_model`,`realisasi_produksi`.`tanggal_selesai` AS `tanggal_selesai` from ((((((`pemesanan` join `pelanggan` on(`pemesanan`.`pelanggan_id` = `pelanggan`.`id`)) join `detail_pemesanan_model` on(`pemesanan`.`id` = `detail_pemesanan_model`.`pemesanan_id`)) join `model` on(`detail_pemesanan_model`.`model_id` = `model`.`id`)) join `perencanaan_produksi` on(`detail_pemesanan_model`.`id` = `perencanaan_produksi`.`id`)) join `realisasi_produksi` on(`perencanaan_produksi`.`id` = `realisasi_produksi`.`perencanaan_produksi_id`)) join `proses_produksi` on(`realisasi_produksi`.`proses_produksi_id` = `proses_produksi`.`id`)) where `proses_produksi`.`id` < 8) union all (select `pemesanan`.`id` AS `id`,`pemesanan`.`penjahit_id` AS `penjahit_id`,`pelanggan`.`nama_pelanggan` AS `nama_pelanggan`,`model`.`nama_model` AS `nama_model`,`pemesanan`.`tanggal` AS `tanggal_selesai` from (((`pemesanan` join `pelanggan` on(`pemesanan`.`pelanggan_id` = `pelanggan`.`id`)) join `detail_pemesanan_model` on(`pemesanan`.`id` = `detail_pemesanan_model`.`pemesanan_id`)) join `model` on(`detail_pemesanan_model`.`model_id` = `model`.`id`)) where `pemesanan`.`pengambilan_id` is null)  ;

-- --------------------------------------------------------

--
-- Structure for view `view_tanggungan_pesanan`
--
DROP TABLE IF EXISTS `view_tanggungan_pesanan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_tanggungan_pesanan`  AS   (select `pemesanan`.`id` AS `id`,`pemesanan`.`penjahit_id` AS `penjahit_id`,`pelanggan`.`nama_pelanggan` AS `nama_pelanggan`,`model`.`nama_model` AS `nama_model`,`realisasi_produksi`.`tanggal_selesai` AS `tanggal_selesai` from ((((((`pemesanan` join `pelanggan` on(`pemesanan`.`pelanggan_id` = `pelanggan`.`id`)) join `detail_pemesanan_model` on(`pemesanan`.`id` = `detail_pemesanan_model`.`pemesanan_id`)) join `model` on(`detail_pemesanan_model`.`model_id` = `model`.`id`)) join `perencanaan_produksi` on(`detail_pemesanan_model`.`id` = `perencanaan_produksi`.`id`)) join `realisasi_produksi` on(`perencanaan_produksi`.`id` = `realisasi_produksi`.`perencanaan_produksi_id`)) join `proses_produksi` on(`realisasi_produksi`.`proses_produksi_id` = `proses_produksi`.`id`)) where `proses_produksi`.`id` < 8) union all (select `pemesanan`.`id` AS `id`,`pemesanan`.`penjahit_id` AS `penjahit_id`,`pelanggan`.`nama_pelanggan` AS `nama_pelanggan`,`model`.`nama_model` AS `nama_model`,`pemesanan`.`tanggal` AS `tanggal_selesai` from (((`pemesanan` join `pelanggan` on(`pemesanan`.`pelanggan_id` = `pelanggan`.`id`)) join `detail_pemesanan_model` on(`pemesanan`.`id` = `detail_pemesanan_model`.`pemesanan_id`)) join `model` on(`detail_pemesanan_model`.`model_id` = `model`.`id`)) where `pemesanan`.`pengambilan_id` is null)  ;

-- --------------------------------------------------------

--
-- Structure for view `view_transaksi_pemesanan_model`
--
DROP TABLE IF EXISTS `view_transaksi_pemesanan_model`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_transaksi_pemesanan_model`  AS SELECT `detail_pemesanan_model`.`id` AS `id_detail_pemesanan_model`, `pemesanan`.`tanggal` AS `tanggal`, `pemesanan`.`pelanggan_id` AS `pelanggan_id`, `pelanggan`.`nama_pelanggan` AS `nama_pelanggan`, `pemesanan`.`penjahit_id` AS `penjahit_id`, `penjahit`.`nama_penjahit` AS `nama_penjahit`, `detail_pemesanan_model`.`model_id` AS `model_id`, `model`.`nama_model` AS `nama_model`, `jenis_model`.`nama_jenismodel` AS `jenis_model`, `detail_pemesanan_model`.`banyaknya` AS `jumlah`, 'PCS' AS `satuan` FROM (((((`pemesanan` join `pelanggan` on(`pemesanan`.`pelanggan_id` = `pelanggan`.`id`)) join `detail_pemesanan_model` on(`pemesanan`.`id` = `detail_pemesanan_model`.`pemesanan_id`)) join `penjahit` on(`pemesanan`.`penjahit_id` = `penjahit`.`id`)) join `model` on(`detail_pemesanan_model`.`model_id` = `model`.`id`)) join `jenis_model` on(`model`.`jenis_model` = `jenis_model`.`id`))  ;

-- --------------------------------------------------------

--
-- Structure for view `view_ukuran`
--
DROP TABLE IF EXISTS `view_ukuran`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ukuran`  AS SELECT `p`.`id` AS `id_pemesanan`, `pm`.`id` AS `id_pemesanan_model`, `uk`.`id` AS `id_ukuran`, `plg`.`nama_pelanggan` AS `nama_pelanggan`, `p`.`tanggal` AS `tanggal`, `m`.`nama_model` AS `nama_model`, `uk`.`ukuran_baju` AS `ukuran_baju`, `uk`.`deskripsi_ukuran` AS `deskripsi_ukuran` FROM ((((`pemesanan` `p` join `detail_pemesanan_model` `pm` on(`p`.`id` = `pm`.`pemesanan_id`)) join `detail_pemesanan_model_ukuran` `uk` on(`pm`.`id` = `uk`.`detail_pemesanan_model_id`)) join `pelanggan` `plg` on(`p`.`pelanggan_id` = `plg`.`id`)) join `model` `m` on(`pm`.`model_id` = `m`.`id`))  ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahan_baku`
--
ALTER TABLE `bahan_baku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bom_model`
--
ALTER TABLE `bom_model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bom_model_detail`
--
ALTER TABLE `bom_model_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bom_standart_ukuran`
--
ALTER TABLE `bom_standart_ukuran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pembelian_bahanbaku`
--
ALTER TABLE `detail_pembelian_bahanbaku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pemesanan_bahanbaku`
--
ALTER TABLE `detail_pemesanan_bahanbaku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pemesanan_model`
--
ALTER TABLE `detail_pemesanan_model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pemesanan_model_ukuran`
--
ALTER TABLE `detail_pemesanan_model_ukuran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `jasa_ekspedisi`
--
ALTER TABLE `jasa_ekspedisi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_model`
--
ALTER TABLE `jenis_model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kolom_rak`
--
ALTER TABLE `kolom_rak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `letak_bahan_baku`
--
ALTER TABLE `letak_bahan_baku`
  ADD PRIMARY KEY (`nama_letak`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model`
--
ALTER TABLE `model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian_bahanbaku`
--
ALTER TABLE `pembelian_bahanbaku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengambilan`
--
ALTER TABLE `pengambilan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penjahit`
--
ALTER TABLE `penjahit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perencanaan_produksi`
--
ALTER TABLE `perencanaan_produksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `proses_produksi`
--
ALTER TABLE `proses_produksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `realisasi_penjahit`
--
ALTER TABLE `realisasi_penjahit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `realisasi_produksi`
--
ALTER TABLE `realisasi_produksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `retur_pemesanan`
--
ALTER TABLE `retur_pemesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_old`
--
ALTER TABLE `users_old`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bahan_baku`
--
ALTER TABLE `bahan_baku`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `bom_model`
--
ALTER TABLE `bom_model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `bom_model_detail`
--
ALTER TABLE `bom_model_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `bom_standart_ukuran`
--
ALTER TABLE `bom_standart_ukuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `detail_pembelian_bahanbaku`
--
ALTER TABLE `detail_pembelian_bahanbaku`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `detail_pemesanan_bahanbaku`
--
ALTER TABLE `detail_pemesanan_bahanbaku`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `detail_pemesanan_model`
--
ALTER TABLE `detail_pemesanan_model`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `detail_pemesanan_model_ukuran`
--
ALTER TABLE `detail_pemesanan_model_ukuran`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jasa_ekspedisi`
--
ALTER TABLE `jasa_ekspedisi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jenis_model`
--
ALTER TABLE `jenis_model`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kolom_rak`
--
ALTER TABLE `kolom_rak`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `model`
--
ALTER TABLE `model`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `pembelian_bahanbaku`
--
ALTER TABLE `pembelian_bahanbaku`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `pengambilan`
--
ALTER TABLE `pengambilan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `penjahit`
--
ALTER TABLE `penjahit`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `perencanaan_produksi`
--
ALTER TABLE `perencanaan_produksi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `proses_produksi`
--
ALTER TABLE `proses_produksi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `realisasi_penjahit`
--
ALTER TABLE `realisasi_penjahit`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `realisasi_produksi`
--
ALTER TABLE `realisasi_produksi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `retur_pemesanan`
--
ALTER TABLE `retur_pemesanan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users_old`
--
ALTER TABLE `users_old`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
